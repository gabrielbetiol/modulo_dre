<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Dre extends MY_Controller{

    public function __construct(){
        $this->load->model('DreModel', 'model');
    }

    public function index(){
        $data['titulo'] = 'Cálculo de DRE';
        $data['rotulo_botao'] = 'Calcular';
        $data['form_subject'] = 'novo_calculo';
        $data['show_form'] = false;
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('fake_form', $data, true);
        
        $html = $this->load->view('main', $data, true);
        $this->show($html);

        
    }

    public function criar(){
        $data['titulo'] = 'Cálculo de DRE';
        $data['show_form'] = false;

        // $data['salvar'] = $this->model->salvar();
        $data['resultado'] = $this->model->calcular();

        $data['rotulo_botao'] = 'Novo Cálculo';
        $data['form_subject'] = 'novo_calculo';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        // $data['formulario'] = $this->load->view('fake_form', $data, true);
        $html = $this->load->view('main', $data, true);
        $this->show($html);
    }

}