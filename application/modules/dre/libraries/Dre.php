<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dre extends Dao {

    function __construct(){
        parent::__construct('dre_calculo');
    }

    public function insert($data, $table = null) {
        // mais uma camada de segurança... além da validação
        $cols = array('nome', 'receita_bruta', 'deducoes_vendas', 'custo_vendas', 'despesas_gerais', 'receitas_diversas');
        $this->expected_cols($cols);

        return parent::insert($data);
    }

    public function calcular()
    {
        //Não fui capaz de efetuar a função atraves da library, ela não carregada, não sei o motivo
    }

}