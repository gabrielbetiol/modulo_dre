<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function form_dre(){
        $this->form_validation->set_rules('nome', 'Nome', 'required|min_length[3]');
        $this->form_validation->set_rules('receita_bruta', 'Receita Bruta', 'required|numeric');
        $this->form_validation->set_rules('deducoes_vendas', 'Deduções das Vendas', 'required|numeric');
        $this->form_validation->set_rules('custo_vendas', 'Custo sobre Vendas', 'required|numeric');
        $this->form_validation->set_rules('despesas_gerais', 'Despesas Gerais', 'required|numeric');
        $this->form_validation->set_rules('receitas_diversas', 'Receitas Diversas', 'required|numeric');
        
        return $this->form_validation->run();
    }

}