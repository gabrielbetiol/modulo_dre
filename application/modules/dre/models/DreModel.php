<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DreModel extends CI_Model{
    
    /**
     * Registra um calculo no bd
     * @param $_POST['nome', 'receita_bruta', 'deducoes_vendas', 'custo_vendas', 'despesas_gerais', 'receitas_diversas']
     * @return boolean true caso ocorra erro de validação
     */
    public function salvar(){
        if(! sizeof($_POST)) return;
        $this->load->library('Validator', null, 'valida');

        if($this->valida->form_dre()){
            $this->load->library('Dre', null, 'dre');
            $data = $this->input->post();
            $this->dre->insert($data);
        }
        else return true;
        
    }

    public function calcular(){
        if(! sizeof($_POST)) return;
        $receita_bruta = $_POST['receita_bruta'];
        $deducoes_vendas = $_POST['deducoes_vendas'];
        $custo_vendas = $_POST['custo_vendas'];
        $despesas_gerais = $_POST['despesas_gerais'];
        $receitas_diversas = $_POST['receitas_diversas'];

        $resultado = $receita_bruta - $deducoes_vendas - $custo_vendas - $despesas_gerais;
        $resultado = $resultado + $receitas_diversas;
        $ir = ($resultado * 15)/100;
        $csll = ($resultado * 0.9)/100;
        $resultado = $resultado - $ir; 
        $resultado = $resultado - $csll;
        $label = "Seu lucro liquido é: R$ ";
        $label = $label." ".$resultado;
        return $label;
    }

}