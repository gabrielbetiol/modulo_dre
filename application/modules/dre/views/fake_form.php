<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_calculo">
    <div class="card">
        <div class="card-header"><h4>Dados da Turma</h4></div>
        <div class="card-body">
            <form method="POST" action="<?= base_url('dre/criar') ?>" class="text-center border border-light p-4">
                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-8">
                        <input type="text" name="receita_bruta" value="<?= set_value('nivel') ?>" class="form-control" placeholder="Receita Bruta">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-8">
                        <input type="text" name="deducoes_vendas" value="<?= set_value('nivel') ?>" class="form-control" placeholder="Deduções das Vendas (-)">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-8">
                        <input type="text" name="custo_vendas" value="<?= set_value('nivel') ?>" class="form-control" placeholder="Custo das Vendas (-)">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-8">
                        <input type="text" name="despesas_gerais" value="<?= set_value('nivel') ?>" class="form-control" placeholder="Despesas Gerais (-)">
                    </div>
                </div>

                <div class="form-row mb-4">
                    <div class="col-md-8">
                        <input type="text" name="receitas_diversas" value="<?= set_value('nivel') ?>" class="form-control" placeholder="Receitas Diversas (+)">
                    </div>
                </div>


                <div class="text-center text-md-right">
                    <button class="btnupload-form btn btn-primary" type="submit"><?= $rotulo_botao ?></button>
                </div>
            </form>
        </div>
    </div>
</div>