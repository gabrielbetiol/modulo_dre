<div class="container mt-5">    
    <?= $topo_pagina ?>
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>

    <div class="row d-flex justify-content-end mt-5">
        <button class="btn btn-primary <?= isset($formulario) ? '' : 'd-none' ?>" type="button" id="collapsebutton" 
        data-toggle="collapse" data-target="#<?= $form_subject ?>" aria-expanded="false" 
        aria-controls="<?= $form_subject ?>"><?= $rotulo_botao ?></button>
    </div>
    
    <div class="row"><?= isset($formulario) ? $formulario : '' ?></div>
    <div class="row mt-3"><?= isset($resultado) ? $resultado : '' ?></div>
    <a class="btn btn-primary <?= isset($resultado) ? '' : 'd-none' ?>" type="button" href="<?= base_url('dre') ?>">Home</a>

</div>