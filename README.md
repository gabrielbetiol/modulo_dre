## AT03 - HMVC
## Módulo Demonstração de Resultado de Execício - DRE

O DRE é o demonstrativo utilizado para que uma empresa possa apurar se obteve lucro ou prejuízo ao final de um determinado período. Nela é confrontado todos os valores das contas de resultados que são as contas onde são apuradas receitas, despesas, custos dentre outras contas.

Sua estrutura é composta de contas que demonstram as entradas de recursos financeiros da empresa versus suas saídas.

Se uma empresa conseguiu vender R$ 100,00 em produtos, por exemplo, e gastou R$ 60,00 para fabricar esse produto e realizar sua venda, ao final do período teremos um lucro de R$ 40,00, ou uma margem de 40%  de lucro sobre vendas (resultado final dividido pelo valor original da venda).

###Arquivos do módulo dentro da pasta modules/dre

####Arquivo do SQL
Arquivo do sql contendo a tabela, na pasta sql, arquivo dre.sql